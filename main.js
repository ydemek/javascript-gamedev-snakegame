var canvas = document.getElementById('myCanvas');
var x = 0;
var y = 0;
var grid = 16;
var currentDirection = 'right';
var ctx = canvas.getContext("2d");
var count = 0;
var fps = 10;
var level = 9;
var score = 0;
var color = "rgba(" + getRandom(0, 255) + "," + getRandom(0, 255) + "," + getRandom(0, 255) + ")";
var snake = {
    x: 160,
    y: 160,
    color: color,

    cells: [],
    maxCells: 5
};

var apple = {
    x: getRandom(0, 40) * grid,
    y: getRandom(0, 25) * grid,
    color: "orange"

};
document.addEventListener("keydown", keyDownHandler);


function keyDownHandler(e) {

    // console.log(e);
    switch (e.key) {
        case 'ArrowRight':
            if (currentDirection == 'left') {
                currentDirection = 'left';
            } else {
                currentDirection = 'right';
            }
            break;
        case 'ArrowLeft':
            if (currentDirection == 'right') {
                currentDirection = 'right';
            } else {
                currentDirection = 'left';
            }
            break;
        case 'ArrowUp':
            if (currentDirection == 'down') {
                currentDirection = 'down';
            } else {
                currentDirection = 'up';
            }
            break;
        case 'ArrowDown':
            if (currentDirection == 'up') {
                currentDirection = 'up';
            } else {
                currentDirection = 'down';
            }
            break;

        default:
            break;
    }

}
function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}




function drawApple(apple) {
    ctx.beginPath();
    ctx.rect(apple.x, apple.y, grid - 1, grid - 1);
    ctx.fillStyle = apple.color;
    ctx.fill();
    ctx.closePath();
}
function drawGameInfo() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "white";
    ctx.fillText("Score: " + score , 10, 30);
    ctx.font = "16px Arial";
    ctx.fillStyle = "white";
    ctx.fillText("Level: " + (level - 8) , 10, 50);
}
function drawSnake(snake) {
    snake.cells.forEach(function (cell, index) {
        ctx.fillStyle = snake.color;
        ctx.fillRect(cell.x, cell.y, grid - 1, grid - 1);


        if (cell.x === apple.x && cell.y === apple.y) {

            snake.maxCells++;
            score++;
            apple.x = getRandom(0, 40) * grid;
            apple.y = getRandom(0, 25) * grid;
        }
        var length = snake.cells.length;
        for (var i = index + 1; i < length; i++) {

            
            if (cell.x === snake.cells[i].x && cell.y === snake.cells[i].y) {
                snake.x = 160;
                snake.y = 160;
                snake.cells = [];
                snake.maxCells = 5;
                snake.dx = grid;
                snake.dy = 0;
                fps = 10;
                score = 0;
                level = 9;
                apple.x = getRandom(0, 40) * grid;
                apple.y = getRandom(0, 25) * grid;
            }

        }
    });
}

function checkForEdges(snake) {
    if (snake.x < 0) {
        snake.x = canvas.width - grid;
    }
    else if (snake.x >= canvas.width) {
        snake.x = 0;
    }

    if (snake.y < 0) {
        snake.y = canvas.height - grid;
    }
    else if (snake.y >= canvas.height) {
        snake.y = 0;
    }
   }




function gameRender() {
    requestAnimationFrame(gameRender);
    if (++count < fps) {
        return;
    }
    count = 0;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    drawGameInfo();
    if (currentDirection == 'right') {
        snake.x += grid;
    }
    if (currentDirection == 'left') {
        snake.x -= grid;
    }
    if (currentDirection == 'up') {
        snake.y -= grid;
    }
    if (currentDirection == 'down') {
        snake.y += grid;

    }
   
   checkForEdges(snake);

    snake.cells.unshift({ x: snake.x, y: snake.y });
    if (snake.cells.length > snake.maxCells) {
        snake.cells.pop();
    }
    drawApple(apple);
    // log for variables
   /*  console.log("level", level);
    console.log("fps", fps);
    console.log("maxCells", snake.maxCells); */
    drawSnake(snake);
    if (snake.maxCells > level) {
        fps -= 1;
        level += 1;
        snake.color = "rgba(" + getRandom(0, 255) + "," + getRandom(0, 255) + "," + getRandom(0, 255) + ")";
    }
   

}
gameRender();
/* setInterval(gameRender, 1000 / fps); // for fps */